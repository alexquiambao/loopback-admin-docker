FROM node:5.0.0

RUN mkdir /src

RUN npm install nodemon -g

WORKDIR /src
ADD package.json package.json
RUN npm install

ADD nodemon.json nodemon.json


RUN git clone --depth=1 --branch=master --recurse-submodules git@github.com:silverbux/adminlte-fireshell.git /app/${BOXEN_CODER_APP_NAME} \
  && cd /app/coder \
  && npm install \
  && bower install --allow-root --config.interactive=false